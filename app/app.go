package app

import (
	"fmt"
	"gitlab.com/valentinsubotka/gocovci"
	"log"
	"os"
	"strconv"
	"strings"
)

type CoverageApp struct {
	Reader gocovci.CoverageReader
	Writer gocovci.CoverageWriter
}

func NewCoverageApp() *CoverageApp {
	writer := gocovci.NewFileCoverageWriter()
	reader := gocovci.NewFileCoverageReader()

	return &CoverageApp{Reader: reader, Writer: writer}
}

func (c *CoverageApp) Run() error {
	var err error
	var out string
	out, err = c.Writer.Create()
	if err != nil {
		fmt.Println(out)
		return fmt.Errorf("failed to create cover profile: %v", err)
	}

	// get tests coverage from coverage profile
	coverage, err := c.Reader.Read()
	if err != nil {
		return fmt.Errorf("failed to get coverage: %v", err)
	}
	log.Println(coverage)

	// get expected coverage from env
	coverageRaw := os.Getenv("GO_COVERAGE")
	log.Println("GO_COVERAGE:", coverageRaw)
	if coverageRaw == "" {
		return nil
	}

	// get percents from coverage and coverageRaw
	coveragePercentRaw, err := parseCoverageRaw(coverageRaw)
	if err != nil {
		return fmt.Errorf("failed to parse coverage percent raw: %v", err)
	}

	coveragePercent, err := c.Reader.ParseCoverage(coverage)
	if err != nil {
		return fmt.Errorf("failed to parse coverage percent: %v", err)
	}

	// compare expected coverage with actual coverage
	log.Println("current coverage:", coveragePercent, "expected coverage:", coveragePercentRaw)

	if coveragePercentRaw > coveragePercent {
		return fmt.Errorf("coverage is less than expected, want: %v, got: %v", coveragePercentRaw, coveragePercent)
	}

	return nil
}

// parseCoverageRaw extract percent from the coverage raw string
func parseCoverageRaw(coveragePercentRaw string) (float64, error) {
	coveragePercentRaw = strings.TrimSuffix(coveragePercentRaw, "%")
	coveragePercent, err := strconv.ParseFloat(coveragePercentRaw, 64)
	if err != nil {
		return 0, fmt.Errorf("failed to parse coverage percent: %v", err)
	}
	return coveragePercent, nil
}
