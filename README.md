# gocovci 
Library to check test coverage.

## Instruction for developers
Used in ci/cd pipeline using docker image. 

Example configuration for gitlab:
```
stages:
  - test

go_coverage:
  image: eazzygroup/gocovci:latest
  before_script: []
  variables:
    GO_COVERAGE: "20" // expected coverage
  stage: test
  script:
    - cd ./path // folder for which you want to check coverage
    - gocovci 
```
You can view the docker image versions here - https://hub.docker.com/r/eazzygroup/gocovci/tags

