# Этап сборки
FROM golang:1.19.7 AS builder

# Установка рабочей директории
WORKDIR /app

# Копирование файлов модуля и суммы
COPY go.mod go.sum ./

# Загрузка зависимостей
RUN go mod download

# Копирование исходного кода
COPY . .

# Сборка приложения
RUN go build -o /gocovci ./cmd/gocovci

# Финальный этап
FROM golang:1.19.7
WORKDIR /
COPY --from=builder /gocovci /sbin/gocovci

