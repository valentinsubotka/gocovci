package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/valentinsubotka/gocovci/app"

	"log"
	"os"
)

func main() {
	var err error
	_ = godotenv.Load()

	app := app.NewCoverageApp()
	err = app.Run()

	_ = os.Remove("coverage.out")

	if err != nil {
		log.Fatal(err)
	}
}
